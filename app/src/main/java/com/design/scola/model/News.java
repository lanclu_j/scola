package com.design.scola.model;

/**
 * - Add description here -
 *
 * @author Jimmy
 */
public class News {
    String theme;
    String name;
    String date;
    String school;
    String description;

    public News (String date, String description, String name, String school, String theme) {
        this.date = date;
        this.description = description;
        this.name = name;
        this.school = school;
        this.theme = theme;
    }

    public String getDate () {
        return date;
    }

    public void setDate (String date) {
        this.date = date;
    }

    public String getDescription () {
        return description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getSchool () {
        return school;
    }

    public void setSchool (String school) {
        this.school = school;
    }

    public String getTheme () {
        return theme;
    }

    public void setTheme (String theme) {
        this.theme = theme;
    }
}
