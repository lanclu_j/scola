package com.design.scola.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.design.scola.DividerItemDecoration;
import com.design.scola.OnFragmentInteractionListener;
import com.design.scola.R;
import com.design.scola.adapter.CollapsableRecyclerAdapter;
import com.design.scola.adapter.PagerAdapter;
import com.design.scola.fragment.AddEventFragment;
import com.design.scola.fragment.AddEventFragmentToo;
import com.design.scola.fragment.FilliereFragment;
import com.design.scola.fragment.PasswordFragment;
import com.design.scola.fragment.SchoolFragment;

import java.lang.reflect.Field;


public class HomeActivity extends AppCompatActivity implements OnFragmentInteractionListener, SearchView.OnQueryTextListener {

    PagerAdapter adapter;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(0xffffffff, PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        SearchView mSearchView = (SearchView) toolbar.findViewById(R.id.search_view);

        try {
            Field f = SearchView.class.getDeclaredField("mSearchButton");
            f.setAccessible(true);

            ImageView iv = ((ImageView) f.get(mSearchView));
            iv.setImageResource(R.drawable.picto_recherche);
            iv.setScaleType(ImageView.ScaleType.FIT_END);
            iv.setPadding(0, 35, -20, 35);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setIcon(getResources().getDrawable(R.drawable.picto_home, getTheme())));
        tabLayout.addTab(tabLayout.newTab().setIcon(getResources().getDrawable(R.drawable.picto_filliere, getTheme())));
        tabLayout.addTab(tabLayout.newTab().setIcon(getResources().getDrawable(R.drawable.picto_event, getTheme())));
        tabLayout.addTab(tabLayout.newTab().setIcon(getResources().getDrawable(R.drawable.picto_profile, getTheme())));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        adapter = new PagerAdapter
            (getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected (TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected (TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void onFragmentInteraction (String s) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);

        switch (s) {
            case "password":
                PasswordFragment passwordFragment = new PasswordFragment();
                transaction.replace(R.id.profile_fragment, passwordFragment).commit();
                break;
            case "events":
                FilliereFragment eventFragment = new FilliereFragment();
                transaction.replace(R.id.eventFragment, eventFragment).commit();
                break;
            case "addEvent":
                AddEventFragment addEventFragment = new AddEventFragment();
                transaction.replace(R.id.eventFragment, addEventFragment).commit();
                break;
            case "addEventToo":
                AddEventFragmentToo addEventFragmentToo = new AddEventFragmentToo();
                transaction.replace(R.id.eventFragment, addEventFragmentToo).commit();
                break;
            case "schoolEvent":
                SchoolFragment schoolFragment = new SchoolFragment();
                transaction.replace(R.id.specialityFragment, schoolFragment).commit();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                if (!getSupportFragmentManager().popBackStackImmediate()) {
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                return true;
            case R.id.action_user:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                LayoutInflater factory = LayoutInflater.from(this);
                final View popupView = factory.inflate(R.layout.search_advanced, null);

                final RecyclerView recyclerView = (RecyclerView) popupView.findViewById(R.id.recyclerView);
                recyclerView.addItemDecoration(new DividerItemDecoration(this));
                LinearLayoutManager layoutManager = new LinearLayoutManager(this);

                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(new CollapsableRecyclerAdapter());

                builder.setView(popupView);
                final AlertDialog alertDialog = builder.create();
                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss (DialogInterface dialog) {
                        item.setIcon(R.drawable.picto_plus_small);
                    }
                });

                alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel (DialogInterface dialog) {
                        item.setIcon(R.drawable.picto_plus_small);
                    }
                });

                Button button = (Button) popupView.findViewById(R.id.button);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick (View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
                item.setIcon(R.drawable.minus_small);
                Resources r = getResources();
                float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 430, r.getDisplayMetrics());
                alertDialog.getWindow().setLayout(1000, (int) px);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextSubmit (String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange (String newText) {
        return false;
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed () {
        if (!getSupportFragmentManager().popBackStackImmediate()) {
            super.onBackPressed();
        }
    }
}
