package com.design.scola.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.design.scola.R;

public class SignInActivity extends AppCompatActivity {

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        Spinner s = (Spinner) findViewById(R.id.spinnerDomain);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
            R.array.domains_array, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(adapter);

        Spinner s1 = (Spinner) findViewById(R.id.spinnerSchool);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
            R.array.schools_array, android.R.layout.simple_spinner_dropdown_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s1.setAdapter(adapter1);

        Spinner s2 = (Spinner) findViewById(R.id.editLevel);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
            R.array.levels_array, android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s2.setAdapter(adapter2);

        findViewById(R.id.loginButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                startActivity(new Intent(SignInActivity.this, LoginConfirmActivity.class));
            }
        });
    }
}
