package com.design.scola.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.design.scola.R;

public class SplashScreenActivity extends AppCompatActivity {

    boolean activated = false;
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);
        findViewById(R.id.splashscreen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (activated) {
                    Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    startActivity(i);
                }
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run () {
                activated = true;
                Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(i);
            }
        }, 2000);
    }
}
