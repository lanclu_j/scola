package com.design.scola.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.design.scola.OnFragmentInteractionListener;
import com.design.scola.R;
import com.design.scola.adapter.NewsAdapter;
import com.design.scola.model.News;

import java.util.ArrayList;
import java.util.List;

public class NewsFragment extends Fragment {

    private RecyclerView mRecyclerView;

    public OnFragmentInteractionListener mListener;

    public static final List<News> newsList;

    static {
        newsList = new ArrayList<>();
        newsList.add(new News("11/02/16",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec",
            "LE BOIS",
            "EPITA" ,
            "Atelier"));
        newsList.add(new News("11/02/16",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec",
            "LE MICRO",
            "Estienne" ,
            "Fab Lab"));
        newsList.add(new News("11/02/16",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec",
            "Elios",
            "ESSEC" ,
            "Conférence"));
        newsList.add(new News("11/02/16",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec",
            "2025",
            "HEC" ,
            "Workshop"));
        newsList.add(new News("11/02/16",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec",
            "JPO",
            "PARIS VI" ,
            "JPO"));
    }
    public NewsFragment () {
        // Required empty public constructor
    }

    public static NewsFragment newInstance (String param1, String param2) {
        NewsFragment fragment = new NewsFragment();
        return fragment;
    }

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mRecyclerView.setLayoutManager( new LinearLayoutManager(this.getActivity()));

        mRecyclerView.setAdapter(new NewsAdapter(this.getContext(), newsList));


    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_news, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        return view;
    }

    @Override
    public void onAttach (Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach () {
        super.onDetach();
        mListener = null;
    }

    public interface ClickListener {
        void OnClick(View v, int position);
    }
}
