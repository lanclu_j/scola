package com.design.scola.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.design.scola.R;
import com.design.scola.model.News;

import java.util.List;
import java.util.Random;

/**
 * - Add description here -
 *
 * @author Jimmy
 */
public class NewsAdapter extends RecyclerView.Adapter<ViewHolder> {

    Context context;
    List<News> newsList;
    Random r;

    public NewsAdapter (Context context, List<News> newsList) {
        this.context = context;
        this.newsList = newsList;
        r = new Random();
    }

    @Override
    public ViewHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder (ViewHolder holder, int position) {
        News n = newsList.get(position);

        holder.school.setText(n.getSchool());
        holder.theme.setText(n.getTheme());
        holder.date.setText(n.getDate());
        holder.name.setText(n.getName());
        holder.description.setText(n.getDescription());
        switch (n.getTheme()) {
            case "Atelier":
                holder.image.setImageDrawable(context.getDrawable(R.drawable.atelier_2));
                break;
            case "Fab Lab":
                holder.image.setImageDrawable(context.getDrawable(R.drawable.fablab_1));
                break;
            case "JPO":
                holder.image.setImageDrawable(context.getDrawable(R.drawable.jpo_1));
                break;
            case "Conférence":
                holder.image.setImageDrawable(context.getDrawable(R.drawable.conference_1));
                break;
            default:
                holder.image.setImageDrawable(context.getDrawable(R.drawable.workshop_1));
                break;
        }
        int i = 0x77000000 + 40 * 0x00010000 + 50 * 0x00000100 + 150;
        holder.image.getDrawable().setColorFilter(i, PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public int getItemCount () {
        return newsList.size();
    }
}

class ViewHolder extends RecyclerView.ViewHolder {
    public TextView school;
    public TextView description;
    public TextView name;
    public TextView date;
    public TextView theme;
    public ImageView image;

    public ViewHolder(View v) {
        super(v);

        school = (TextView) v.findViewById(R.id.school);
        description = (TextView) v.findViewById(R.id.description);
        name = (TextView) v.findViewById(R.id.name);
        date = (TextView) v.findViewById(R.id.date);
        theme = (TextView) v.findViewById(R.id.theme);
        image = (ImageView) v.findViewById(R.id.image);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}