package com.design.scola.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.design.scola.fragment.EventFragment;
import com.design.scola.fragment.NewsFragment;
import com.design.scola.fragment.ProfileFragment;
import com.design.scola.fragment.SpecialityFragment;

/**
 * - Add description here -
 *
 * @author Jimmy
 */
public class PagerAdapter extends FragmentStatePagerAdapter {

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public NewsFragment tab1;
    public SpecialityFragment tab2;
    public EventFragment tab3;
    public ProfileFragment tab4;

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                if (tab1 == null)
                tab1 = new NewsFragment();
                return tab1;
            case 1:
                if (tab2 == null)
                tab2 = new SpecialityFragment();
                return tab2;
            case 2:
                if (tab3 == null)
                tab3 = new EventFragment();
                return tab3;
            case 3:
                if (tab4 == null)
                tab4 = new ProfileFragment();
                return tab4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}