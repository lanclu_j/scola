package com.design.scola.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.design.scola.R;
import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.github.aakira.expandablelayout.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * - Add description here -
 *
 * @author Jimmy
 */
public class CollapsableRecyclerAdapter  extends RecyclerView.Adapter<CollapsableViewHolder> {

    private Context context;
    private SparseBooleanArray expandState = new SparseBooleanArray();
    static final List<String> data = new ArrayList<>();

    static {
        data.add("Écoles");
        data.add("Évènements");
        data.add("Domaines");
        data.add("Autour de moi");
        data.add("Prix");
    }

    public CollapsableRecyclerAdapter() {

        for (int i = 0; i < data.size(); i++) {
            expandState.append(i, false);
        }
    }

    @Override
    public CollapsableViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new CollapsableViewHolder(LayoutInflater.from(context)
            .inflate(R.layout.search_advanced_item, parent, false), context);
    }

    @Override
    public void onBindViewHolder (final CollapsableViewHolder holder, final int position) {
        final String item = data.get(position);

        switch (item) {
            case "Écoles" :
                holder.stub.setLayoutResource(R.layout.school_filter);
                break;
            case "Évènements" :
                holder.stub.setLayoutResource(R.layout.event_filter);
                break;
            case "Domaines" :
                holder.stub.setLayoutResource(R.layout.domain_filter);
                break;
            case "Autour de moi" :
                holder.stub.setLayoutResource(R.layout.nearby_filter);
                break;
            case "Prix" :
                holder.stub.setLayoutResource(R.layout.price_filter);
                break;
            default:
                break;
        }
        holder.stub.inflate();

        final Resources resource = context.getResources();
        holder.textView.setText(item);
        holder.itemView.setBackgroundColor(resource.getColor(R.color.colorPrimary, context.getTheme()));
        holder.expandableLayout.setBackgroundColor(0xffffffff);
        holder.expandableLayout.setInterpolator(Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR));
        holder.expandableLayout.setExpanded(expandState.get(position));
        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen () {
                createRotateAnimator(holder.buttonLayout, 0f, 90f).start();
                expandState.put(position, true);
            }

            @Override
            public void onPreClose () {
                createRotateAnimator(holder.buttonLayout, 90f, 0f).start();
                expandState.put(position, false);
            }


        });

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                onClickButton(holder.expandableLayout);
            }
        };

        holder.buttonLayout.setRotation(expandState.get(position) ? 90f : 0f);
        holder.buttonLayout.setOnClickListener(onClickListener);
        holder.textView.setOnClickListener(onClickListener);
    }

    private void onClickButton(final ExpandableLayout expandableLayout) {
        expandableLayout.toggle();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }
}

class CollapsableViewHolder extends RecyclerView.ViewHolder {
    public TextView textView;
    public RelativeLayout buttonLayout;
    public ExpandableRelativeLayout expandableLayout;
    public  ViewStub stub;

    public CollapsableViewHolder(View v, Context context) {
        super(v);
        textView = (TextView) v.findViewById(R.id.textView);
        buttonLayout = (RelativeLayout) v.findViewById(R.id.button);
        expandableLayout = (ExpandableRelativeLayout) v.findViewById(R.id.expandableLayout);

        stub = (ViewStub) v.findViewById(R.id.layout_stub);
    }
}
